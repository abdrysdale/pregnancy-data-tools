# Dockerfile to create python environment
FROM python:3.10.10

# Installs system packages
RUN apt update && apt install -y \
sqlite3 \
tesseract-ocr \
tesseract-ocr-eng \
libgl1
RUN python -m pip install --upgrade pip setuptools wheel
RUN python -m pip install poetry

# Sets up the /app/ directory with the correct scripts
RUN mkdir /app
RUN mkdir /data
COPY pyproject.toml /app/
COPY poetry.lock /app/

# Sets up the environment
WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}
ENV toml="/app/data/build_database.toml"
ENV log="WARNING"

# Installs python dependencies
RUN poetry config virtualenvs.create false
RUN poetry update && poetry install --no-interaction

# Copies over the source directory
COPY src /app/src

# Runs the build database script
CMD ["sh", "-c", "python /app/src/build_database.py -c ${toml} -l ${log}"]
