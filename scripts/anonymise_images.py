"""A script to anonymise a directory of images"""

#! /usr/bin/env python

# Python imports
import sys
import os

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

from src import image_processing

def main():
    """Main function for batch masking of images"""

    src_dir = "/data/tmp/"              # Paths are within the container
    dst_dir = "/data/us-data-anon/"
    mask = { "type" : "templates", "mask" : "/app/src/us_templates.toml" }
    skip_df = [
        "REPORTS",
		"SECTRA",
		"INDEX.HTM",
		"README.TXT",
		"HTM",
		"IMAGES",
		"THUMNAIL.JPG",
        "THUMNAIL_1.JPG",
        "edj 15-01-2022.csv",
        "non_anon_data(old).xls",
        "Recent_edit.csv",
        "Recent_edit.xlsx",
        "old",
    ]
    preserve_structure = True

    image_processing.batch_mask_images(
            src_dir,
            dst_dir,
            mask=mask,
            skip_df=skip_df,
            preserve_structure=preserve_structure,
    )

if __name__ == "__main__":
    main()
