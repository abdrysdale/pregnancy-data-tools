#! /usr/bin/env sh

container_name="preg_data:latest"
data_dir="/mnt/veracrypt2/"
port="8888:8888"

args="${@}"
run=0

while [ -n "${1}" ]
do
case "${1}" in
-b | --build) 
	run=1
	docker build -t ${container_name} .
	exit;;
-l | --lab)
	run=1
    docker run -it -v "$(pwd)":/app -v ${data_dir}:/data -p ${port} ${container_name} \
		jupyter lab --ip 0.0.0.0 --no-browser --allow-root
	exit;;
-s | --shell)
	run=1
	docker run -it -v "$(pwd)":/app -v "${data_dir}":/data ${container_name} sh;;
-e | --exec)
	run=1
    docker run -v "$(pwd)":/app -v ${data_dir}:/data ${container_name} "${@:2}";;
-h | --help)
	run=1
	echo "./run.sh [OPTION] [ARGS]"
	echo ""
	echo "This is a simple script for ease of running container commands."
	echo "By default it binds the current directory to /app in the container and the data_dir to"
	echo "/data in the container."
	echo ""
	echo "Options:"
	echo "--build   Builds a docker container from the Dockerfile."
	echo "--lab     Launches a jupyter lab instance using docker on port ${port}."
	echo "--shell   Launches a docker container binding the current and data directory".
	echo "--exec    Passes ARGS directory to the docker container."
	echo "--help    Displays this message."
	echo ""
	echo "If no option is specified, runs ARGS with python3."
	echo ""
	echo "Current variables:"
	echo "container_name    ${container_name}"
	echo "data_dir          ${data_dir}"
	echo "port              ${port}"
	echo ""
	echo "Variables are intended to be edited to reflect user setup.";;

esac
shift
done

if [ ${run} -eq 0 ]
then
    docker run -v "$(pwd)":/app -v ${data_dir}:/data ${container_name} python3 ${args}
fi
