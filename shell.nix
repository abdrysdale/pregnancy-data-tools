{ pkgs ? import <nixpkgs> {} }:

let
  container_name = "preg_data:latest";
  data_dir = "/mnt/veracrypt2/";
  port="8888:8888";
in
pkgs.mkShell {
  buildInputs = [
    pkgs.black
    pkgs.nbstripout
    pkgs.python310Packages.numpy
    pkgs.python310Packages.pandas
    pkgs.python310Packages.opencv4
  ];
  shellHook = ''
    alias build="docker build -t ${container_name} ."
    alias run="docker run -v "$(pwd)":/app -v ${data_dir}:/data ${container_name}"
    alias shell="docker run -it -v "$(pwd)":/app -v ${data_dir}:/data ${container_name} sh"
    alias lab="docker run -it -v "$(pwd)":/app -v ${data_dir}:/data -p ${port} ${container_name} jupyter lab --ip 0.0.0.0 --no-browser --allow-root"
	alias gci="./hooks/pre-commit && git commit -n"
  '';
}
  
