""" Tests for the build_database.py file."""

# /usr/bin/env python

# Python imports
import sys
import time
import os
# TESSDATA_PREFIX is always found to be wrong for me, so force it here:
os.environ['TESSDATA_PREFIX'] = r'C:\Program Files\Tesseract-OCR\tessdata'

# Module imports
import toml
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from src import cv_functions
from src import build_database
import numpy as np
from PIL import Image


# image_path = "E:/us-data-anon/5388/IHE_PDI/0000CFEB/AACEC5F7/AAA0DC5C/0000EF40/EE99EFE9.JPG"
# image = np.array(Image.open(image_path))
# signal, t_axis = cv_functions.segment_doppler(image)


# i=0


def test_main():
    """Tests the main function"""

    toml_file = "tests/test_data/build_database.toml"
    not_linked_file = toml.load(toml_file)["database_parameters"]["log_not_linked"]
    if os.path.exists(not_linked_file):
        os.remove(not_linked_file)

    assert build_database.main(
        toml_file,
        log_level="DEBUG",
    )

    #assert not os.path.exists(not_linked_file)


if __name__ == "__main__":

    start_time = time.time()
    logger.debug("Testing build_database.py ...")
    #test_create_database_from_csv()
    #test_add_images_table_to_database()
    test_main()

    elapsed_time = time.time() - start_time
    print(f"Elapsed time: {elapsed_time:.2f} seconds")
    #logger.info("Great news, build_database.py passed all tests!")