# Pregnancy Data Tools

A set of data extraction tools used to create a SQL database of patient metadata and images.

## Installation

### Using Docker (recommended)

1. Following the [official documentation](https://docs.docker.com/get-docker/), install docker.

2. Clone this repository and change directory into the repository:
    `git clone https://gitlab.com/abdrysdale/pregnancy-data-tools.git`

	``cd pregnancy-data-tools``

3. Build the docker image:
	``docker build -t IMAGE_NAME .``

4. Check that the image has been built:
	``docker image ls``
	Under the `REPOSITORY` there should be an image named `IMAGE_NAME`.

5. Ensure the tests pass with:
	`docker run -v "$(pwd)":/app IMAGE_NAME pytest`


## Usage

Build the database with the following command:
```
	docker run \
	-v "$(pwd)":/app \
	-v PATH_TO_IMAGE_DIR:/data \
	-e toml=CONTAINER_TOML_PATH \
	-e log=LOG_LEVEL \
	IMAGE_NAME
```

>- `PATH_TO_IMAGE_DIR` is the path to the directory containing the image directory.
>- `CONTAINER_TOML_PATH` is the path to the configuration (toml) file within the container.
>- `LOG_LEVEL` is the log level to passed to the loguru logger.

That is, if one has the following layout (assuming the current directory is the directory of git repository):
```
	data/
		build_database.toml

	/path/to/image/directory/
```

The following command would be appropriate for a container named CONTAINER_NAME:

```
docker run -v "$(pwd)":/app -v /path/to/image/directory/:/data -e toml=/app/data/build_databse.toml CONTAINER_NAME
```

> Ensuring that in `build_database.toml`, `"image_dir" = "/data"`.

- See [shell.nix](shell.nix) for some useful docker aliases.
- It's worth noting that although [nix](https://nixos.org/manual/nixpkgs/stable/#overview-of-nixpkgs) is used for the development of the project it is not needed for general use and is merely used as a development environment.
	- See the `pkgs.mkShell.buildInputs` for an incomplete list of packages that aid development.

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md) for details.

## Documentation

For API documentation, Please see the full documentation [here](docs/build/index.html).
Note that the documentation is under active development.
