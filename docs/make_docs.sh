#! /usr/bin/env sh

sphinx-apidoc -o docs/source/ .
sphinx-build -b html docs/source docs/build/
