""" A python script containing logging functions """

# Python imports
import time
import sys

# Module imports
from loguru import logger

# Sets up a logger
logger.remove()
handle_id = logger.add(
    sys.stderr,
    level="WARNING",
    colorize=True,
    backtrace=True,
    diagnose=True,
)


# A simple debugging decorator
# Code snippet taken from:
# https://realpython.com/inner-functions-what-are-they-good-for/
def debug(func, time_it=False):
    """A function to be used as a debugging decorator

    Args:
        func (function) : The function to be debugged.
        time_it (bool, optional) : Whether to time the function.
            Defaults to False.
    """

    def _debug(*args, **kwargs):
        # Times the function
        if time_it:
            start = time.time()

        # Runs the function and gets the result
        logger.debug(f"Calling {func.__name__}(args: {args}, kwargs: {kwargs}) ...")
        result = func(*args, **kwargs)

        # Gets the run time and logs output
        if time_it:
            end = time.time()
            logger.debug(f"{func.__name__} completed in {end - start}s")
        logger.debug(f"{func.__name__}(args: {args}, kwargs: {kwargs} -> {result}")
        return result

    return _debug
