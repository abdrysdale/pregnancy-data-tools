"""A script and necessary functions that builds a local database."""

#! /usr/bin/env python

# Python imports
import os
import sys
import traceback
import multiprocessing as mp
import math
import hashlib

# Module imports
import numpy as np
import pandas as pd
import click
import toml
from loguru import logger
from tqdm import tqdm
from PIL import Image
import usseg
import time
import cv2
import pickle

# Local imports
from src import sqlite_functions
from src import cv_functions
from src import log
from src import gen_hash
from src import image_processing
from src import missing_data
from src import match_id


def create_database_from_csv(dpd):
    """Creates a database and populates with a csv file.

    Args:
        dpd (dict) : A Database Parameter Dictionary containing the following:
            csv_path (str, optional) : The filename of the csv file
                containing the tabular patient data.
                Defaults to 'patient_metadata.csv'.
            database_path (str, optional) : The filename for the database.
                Defaults to 'patient_database.sqlite3'.
            table_name (str, optional) : The name of the table where the csv data will be stored.
                Defaults to 'tabular_data'.
            hash_id (bool, optional) : If True, hashes the patient_id.
                Defaults to True.
            overwrite (bool, optional) : If True, overwrites the database creating
                a new database from scratch.  Defaults to True.

    Returns:
        connection (database connection) : SQLite database connection.
    """

    # Deletes the database if it exists
    # This is very inefficient, it should look for the database and present the user with options:
    # Tabular data:
    # 1) Overwrite
    # 2) Keep
    if os.path.exists(dpd["database_path"]):
        if dpd["overwrite"]:
            os.remove(dpd["database_path"])
            logger.debug(f"Deleted {dpd['database_path']} for rebuild.")

    # Loads the csv file into a pandas dataframe.
    connection = sqlite_functions.connect_to_database(database=dpd["database_path"])
    if dpd["csv_path"].split(".")[-1] == "csv":
        tab_df = pd.read_csv(dpd["csv_path"])
    else:
        tab_df = pd.read_excel(dpd["csv_path"])

    # Hashes the patient ID
    if dpd["hash_id"]:
        key_dict = gen_hash.get_keydict()

        with open(dpd["hash_csv"], "w", encoding="utf-8") as file:
            file.write("ID,Hash\n")

        with open(dpd["hash_csv"], "a", encoding="utf-8") as file:
            for i, patient_id in enumerate(tab_df[dpd["id_field"]]):
                tab_df.loc[i, dpd["id_field"]] = gen_hash.hash_data(
                    patient_id,
                    key_dict,
                )
                file.write(f"{patient_id},{tab_df[dpd['id_field']][i]}\n")

    # Fills in missing data
    if dpd["fill_in_gest"]:
        tab_df = missing_data.fill_missing_gest(
            tab_df,
            dpd,
            prev_visit=dpd["prev_visit_fill_in"],
        )

        # Converts key values to the correct format
        # pylint: disable=unsubscriptable-object
        tab_df[dpd["gest_field"]][pd.notna(tab_df[dpd["gest_field"]])] = tab_df[
            dpd["gest_field"]
        ][pd.notna(tab_df[dpd["gest_field"]])].astype(int)

    # Writes the dataframe to SQLite3 database
    if_exists = "replace" if dpd["overwrite"] else "append"
    tab_df.to_sql(  # pylint: disable=no-member
        dpd["table_name"],
        connection,
        if_exists=if_exists,
        index="False",
    )
    return connection


def add_images_table_to_database(
    connection,
    image_table_name="images",
    table_dict=None,
):
    """Adds an empty table for images to a database connection

    Args:
        connection (database connection) : SQLite database connection.
        image_table_name (str, optional) : The name for the images table.
            Defaults to 'images'.
        table_dict (dict) : A dictionary containing the "table header" : "data type".
            If None, defaults to:
            table_dict = {
                "patientid" : "INTEGER",
                "dateofexam" : "TEXT",
                "usgestdays" : "INTEGER",
                "image_number" : "INTEGER",
                "image_type" : "INTEGER",
                "image" : "BLOB",
            }
            Defaults to None.

    Returns
        return value (bool) : True if successful, False otherwise.
    """

    if connection is None:
        logger.critical("No SQL connection passed.\nFailed to create images table.")
        return False

    if isinstance(table_dict, type(None)):
        table_dict = {
            "patientid": "INTEGER",
            "dateofexam": "TEXT",
            "usgestdays": "INTEGER",
            "image_number": "INTEGER",
            "image_type": "INTEGER",
            "image": "BLOB",
        }

    # Statement to create the image table.
    create_statement = "CREATE TABLE\n" f"{str(image_table_name)}("
    for key in list(table_dict.keys()):
        create_statement += f"{key} {table_dict[key]},"
    create_statement = create_statement[:-1] + ")"

    # Creates the image table
    connection.execute(create_statement)
    connection.commit()

    return True


def get_metadata_from_image_path_parallel(args):
    """Wrapper around get_metadata_from_image_path for parallel implementation

    Args:
        args (tuple) : Tuple of (image_path, config_file_dict, log_file).
    """
    return get_metadata_from_image_path(*args)


def get_metadata_from_image_path(
    image_path,
    config_file_dict=None,
    log_file="failed_images.log",
    date_sep="/",
    extract_doppler=True,
):
    """Extracts the metadata from an image path.

    Args:
        image_path (str) : Path to the image to be added to the database.
        config_file_dict (dict, optional) : Default metadata config file.
            If None, loads default config file. Defaults to None.
        log_file (str, optional) : Log file for failed images.
        date_sep (str, optional) : What to replace the date joining character with.
            Defaults to "/".
        extract_doppler (bool, optional) : If True, extracts the doppler signal.
                Defaults to True.

    Returns:
        metadata_dict (dict) : A dictionary of the image metadata.

    """
    # Loads the image
    image = np.array(Image.open(image_path))
    logger.debug(f"Loaded image from: {image_path}")

    # Sets the default configuration file dictionary
    if config_file_dict is None:
        config_file_dict = cv_functions.load_default_config_file_dict()
        logger.debug("Config not specified, loading the default configuration.")
    logger.debug(f"Using the following config:\n{config_file_dict}")

    def _log_and_handle_err():
        logger.error(f"Failed to segment ultrasound from {image_path}")
        with open(log_file, mode="a", encoding="utf-8") as file:
            file.write(f"{image_path} failed:\t{traceback.format_exc()}\n")
        return "N/A","N/A", "N/A"

    # Extracts the segmentation
    try:
        df, signal, t_axis = cv_functions.segment_doppler(image)

    except ValueError:
        df, signal, t_axis = _log_and_handle_err()

    except SystemError as exc:
        if str(exc) == "tile cannot extend outside image":
            _, signal, t_axis = _log_and_handle_err()
        else:
            raise SystemError from exc

    except ZeroDivisionError:
        df, signal, t_axis = _log_and_handle_err()

    except IndexError:
        df, signal, t_axis = _log_and_handle_err()

    except cv2.error:
        df, signal, t_axis = _log_and_handle_err()


    # Gets the metadata
    try:
        image_metadata = cv_functions.get_us_metadata(
            image,
            config_file_dict,
            date_sep=date_sep,
        )

        
        def find_prefix(word):
            prefixes = ['Lt', 'Rt', 'Umb','DV']
            for prefix in prefixes:
                if prefix in word:
                    return prefix
            return 'NA'
        
        if extract_doppler:
            image_metadata["signal"] = signal
            image_metadata["t_axis"] = t_axis
            try:
                
                # Function to find the prefix

                # Extract prefix from the first entry in 'Word' column
                image_metadata["prefix"] = find_prefix(df['Word'].iloc[0])
                if image_metadata["prefix"] == "DV":
                    image_metadata["PS"] = df.loc[df['Word'] == 'DV-S', 'Value'].values[0]
                    image_metadata["ED"] = df.loc[df['Word'] == 'DV-D', 'Value'].values[0]
                    image_metadata["a"] = df.loc[df['Word'] == 'DV-a', 'Value'].values[0]
                else:
                    image_metadata["PS"] = df.loc[df['Word'].str.contains('PS'), 'Value'].values[0] if df['Word'].str.contains('PS').any() else 0 
                    image_metadata["ED"] = df.loc[df['Word'].str.contains('ED'), 'Value'].values[0] if df['Word'].str.contains('ED').any() else 0
                    image_metadata["a"] = 0

                image_metadata["PI"] = df.loc[df['Word'].str.contains('PI'), 'Value'].values[0] if df['Word'].str.contains('PI').any() else 0
                image_metadata["RI"] = df.loc[df['Word'].str.contains('RI'), 'Value'].values[0] if df['Word'].str.contains('RI').any() else 0
                image_metadata["MD"] = df.loc[df['Word'].str.contains('MD'), 'Value'].values[0] if df['Word'].str.contains('MD').any() else 0
                image_metadata["TAmax"] = df.loc[df['Word'].str.contains('TAmax'), 'Value'].values[0] if df['Word'].str.contains('TAmax').any() else 0
                image_metadata["HR"] = df.loc[df['Word'].str.contains('HR'), 'Value'].values[0] if df['Word'].str.contains('HR').any() else 0
            except:
                image_metadata["prefix"] = 'N/A'
                image_metadata["PS"] = 0
                image_metadata["ED"] = 0
                image_metadata["a"] = 0
                image_metadata["PI"] = 0
                image_metadata["RI"] = 0
                image_metadata["MD"] = 0
                image_metadata["TAmax"] = 0
                image_metadata["HR"] = 0
            
        return image_metadata

    # If extracting the metadata fails then returns False.
    # Saves the failed image path to a log file.
    except ValueError:
        logger.error(f"Failed get metadata from {image_path}.")
        with open(log_file, mode="a", encoding="utf-8") as file:
            file.write(f"{image_path} failed:\t{traceback.format_exc()}\n")

        return False


def mask_image(image, field="patient_id_coords"):
    """Loads and masks the image

    Args:
        image (ndarray) : A numpy array of the image.
        field (str, optional) : The field to mask from the default template.
            Defaults to 'patient_id_coords'.

    Returns:
        masked_image : An image with masked a field.

    """
    config_file_dict = cv_functions.load_default_config_file_dict()
    template_list = cv_functions.load_us_templates(
        config_file_dict["template_toml_file"]
    )
    institute_template = cv_functions.determine_image_template(image, template_list)
    masked_image = image_processing.mask_patient_data(
        image,
        mask={"type": "coords", "mask": institute_template[field]},
    )

    return masked_image


def batch_add_images_and_metadata_to_database(
    connection,
    image_list,
    metadata_list,
    table_dict,
    database_params,
):  # pylint: disable=too-many-locals
    """Batch adds the images and metadata to the database.

    Args:
        connection (database connection) : SQLite database connection.
        image_list (list) : A list of numpy array images.
        metadata_list (list) : A list of dictionaries containing the image metadata.
        table_dict (dict) : A dictionary of the SQL tabular data fields.
        database_params (dict) : A dictionary of the general database parameters.

    Returns:
        return value (bool) : True if successful, False otherwise.
    """

    key_dict = gen_hash.get_keydict()
    log_file = database_params["log_failed_images"]

    logger.debug("Saving images to database...")
    for i, image_path in enumerate(image_list):
        # Loads the image and masks the image
        try:
            image = mask_image(
                np.array(Image.open(image_path)),
                field="patient_id_coords",
            )
        except ValueError:
            logger.error(f"Failed get metadata from {image_path}.")
            with open(log_file, mode="a", encoding="utf-8") as file:
                file.write(f"{image_path} failed:\t{traceback.format_exc()}\n")
            next(image_list)

        image_hash = hashlib.sha3_512(image).hexdigest()

        # Checks if the image is in the database
        in_db = sqlite_functions.get_from_db(
            connection,
            database_params["image_table_name"],
            "image_hash",
            image_hash,
        )

        if not in_db:
            # Gets the metadata from the list
            image_metadata = metadata_list[i]

            # Adds an image number
            image_metadata["image_number"] = 0
            image_metadata["image_hash"] = image_hash
            image_metadata["image_path"] = image_path

            # Matches the patient ID
            pid = match_id.match_id(
                image_metadata, key_dict, connection, database_params
            )
            if not pid:
                with open(
                    database_params["log_not_linked"], "a", encoding="utf-8"
                ) as file:
                    file.write(
                        f"{image_path}: "
                        f"{[image_metadata[field] for field in database_params['match_fields']]}\n"
                    )
            else:
                image_metadata["patient_id"] = pid

            # Hashes ID
            if database_params["hash_id"]:
                image_metadata["patient_id"] = gen_hash.hash_data(
                    image_metadata["patient_id"],
                    key_dict,
                )

            # Adds image and metadata to database
            sqlite_functions.add_image_data_to_database(
                connection,
                image_metadata,
                image,
                table_name=database_params["image_table_name"],
                headers=list(table_dict.keys()),
            )

        else:
            # Ensure that in_db is a list containing a row from the database
            if not isinstance(in_db, list) or not in_db:
                logger.error("Expected in_db to be a list with database rows")
                # Print the value of in_db
                print(f"Value of in_db: {in_db}")
                # Raise an exception to stop execution and provide a traceback
                raise ValueError(f"Expected in_db to be a list with database rows, but got {type(in_db)} instead.")

            path_loc = [
                i for i, k in enumerate(table_dict.keys()) if k == "image_path"
            ][0]
            try:
                duplicate_path = in_db[0][path_loc]
            except TypeError as e:
                logger.error(f"Error retrieving duplicate path: {e}")
                # Print the value of in_db and path_loc
                print(f"Value of in_db: {in_db}")
                print(f"Value of path_loc: {path_loc}")
                # Raise an exception to stop execution and provide a traceback
                raise

            with open(
                database_params["log_duplicate_images"], "a", encoding="utf-8"
            ) as file:
                file.write(f"{image_path} is a duplicate of {duplicate_path}\n")
            logger.warning(
                f"Skipping '{image_path}' as already in the database.\n"
                f"Hash used: {image_hash}"
            )


        # Adds a hash of the image into the database and adds image if unique

    return True


def get_sucessful_metadata(
    image_list,
    log_file="failed_images.log",
    debug=True,
    date_sep="/",
):
    """Returns only the sucessfully extracted metadata from images.

    Args:
        image_list (list) : A list of image paths.
        log_file (str, optional) : Path to the failed images log file.
            Defaults to "failed_images.log".
        debug (bool, optional) : If debug is True, will not use parallelisation.
            Defaults to False.
        date_sep (str, optional) : What to replace the date joining character with.
            Defaults to "/".

    Returns:
        (
            metadata_list_success (list) : A list of successfully extracted metadata.
            image_list_success (list) : Image paths of successful images.
        )
    """
    if len(image_list) == 0:
        logger.critical(f"Image list {image_list} is empty!")
        raise ValueError("Empty image list")

    no_usable_processes = mp.cpu_count() - 1
    logger.info(
        f"Building a database of {len(image_list)} images "
        f"using {no_usable_processes} processes."
    )

    arg_list = []
    for path in image_list:
        arg_list.append((path, None, log_file, date_sep))

    if not debug:
        with mp.Pool(no_usable_processes) as pool:
            metadata_list = list(  # List (of dicts) output
                tqdm(  # Progress bar
                    pool.imap(  # Multiprocessing
                        get_metadata_from_image_path_parallel,
                        arg_list,
                    ),
                    total=len(image_list),
                    desc="Adding image to database",
                )
            )
    else:
        metadata_list = []
        for i in tqdm(range(len(image_list))):
            metadata_list.append(get_metadata_from_image_path_parallel(arg_list[i]))

    # Removes failed images
    metadata_list_success = []
    image_list_success = []
    for i, metadata in enumerate(metadata_list):
        if metadata:
            metadata_list_success.append(metadata)
            image_list_success.append(image_list[i])
    logger.info(f"{len(image_list_success)/len(image_list)} metadata extracted.")

    return metadata_list_success, image_list_success


def image_in_db(connection, image_path, database_params):
    """Check if image is in the database.

    Args:
        connection (database connection) : SQLite database connection.
        image_path (str) : Path to the image.
        database_params (dict) : A dictionary of the general database parameters.

    Returns:
        return value (bool) : True if in database, False otherwise.
    """
    key_dict = gen_hash.get_keydict()
    log_file = database_params["log_failed_images"]

    try:
        image = mask_image(
            np.array(Image.open(image_path)),
            field="patient_id_coords",
        )
    except ValueError:
        logger.error(f"Failed get metadata from {image_path}.")
        with open(log_file, mode="a", encoding="utf-8") as file:
            file.write(f"{image_path} failed:\t{traceback.format_exc()}\n")
        return False

    image_hash = gen_hash.hash_data(image, key_dict)

    # Checks if the image is in the database
    return bool(
        sqlite_functions.get_from_db(
            connection,
            database_params["image_table_name"],
            "image_hash",
            image_hash,
        )
    )


def add_images_to_database(
    connection,
    database_params,
    table_dict,
):
    """

    Args:
        connection (database connection) : SQLite database connection.
        database_params(dict) : A dictionary containing the
            table names, columns and fields to get a database match from.
            Expected fields are:
                "table_name"
                "match_cols"
                "match_fields"
                "image_dir" : path to the directory containing all the images. Defaults to 'images'.
                "cores" : Number of cores to use. If None, will use all available cores - 1.
                        Defaults to None.
                "batch_size" : Size of the batch. If None, defaults to the number of cores.
                        Defaults to None.
        table_dict (dict) : A dictionary of SQL table names and datatypes.

    Returns
        return value (bool) : True if successful, False otherwise.

    """
    image_dir = database_params.get("image_dir", "images")
    cores = database_params.get("cores", None)
    batch_size = database_params.get("batch_size", None)
    likley_us_image_list = database_params.get("likley_us_image_list",None)

    # Gets a list of images
    image_list = []

    # Gets the image and adds to the list
    ## Check here for likley US images 
    start_time = time.time()
    filenames_is_us = usseg.get_likely_us(image_dir,likley_us_image_list,True)
        

    # Replace this with the path to your pickle file
    pickle_file_path = 'E:/us-data-anon/0000/US_filename.pkl'

    # Load the dictionary from the pickle file
    with open(pickle_file_path, 'rb') as file:
        file_paths_dict = pickle.load(file)

    # Extract all file paths into a single list
    filenames_is_us = []
    for file_list in file_paths_dict.values():
        filenames_is_us.extend(file_list)

    print(filenames_is_us)

    image_list = filenames_is_us
    elapsed_time = time.time() - start_time
    print(f"Elapsed time: {elapsed_time:.2f} seconds")
  
    # For each image file, gets metadata and saves to database
    # Uses a multiprocessing implementation
    # Process the images in batch sizes equal to the number of cores - 1
    # Number of processes:
    if batch_size is None:
        n_p = mp.cpu_count() - 1 if cores is None or cores > mp.cpu_count() else cores
    else:
        n_p = batch_size

    n_b = math.ceil(len(image_list) / n_p)  # Number of batches
    for _ in tqdm(range(n_b), desc="Batch number:"):
        image_batch, image_list = image_list[:n_p], image_list[n_p:]
        image_batch = [
            p for p in image_batch if not image_in_db(connection, p, database_params)
        ]
        if image_batch:
            metadata_list_success, image_list_success = get_sucessful_metadata(
                image_batch,
                log_file=database_params["log_failed_images"],
                debug=database_params["debug"],
                date_sep=database_params["date_format"].split("%")[1][-1],
            )

            # Adds all the images and metadata to the database
            batch_add_images_and_metadata_to_database(
                connection,
                image_list_success,
                metadata_list_success,
                table_dict,
                database_params,
            )

    return True


@log.debug
def build_database(database_params=None, table_dict=None):
    """Main function to create the database

    Args:
        database_params (dict, optional) : A dictionary containing parameters
            to build the database. If None, uses default parameter values.
            Required fields:
            csv_filename (str, optional) : The filename of the csv file
                containing the tabular patient data.
                Defaults to 'patient_metadata.csv'.
            database_filename (str, optional) : The filename for the database.
                Defaults to 'patient_database.sqlite3'.
            data_directory (str, optional) : The directory where the csv file is located.
                Defaults to 'data'.
            table_name (str, optional) : The name of the table where the csv data will be stored.
            image_table_name (str, optional) : The name for the images table.
                Defaults to 'images'.
            image_dir (str, optional) : The path to the directory containing all the images.
                Defaults to 'images'.

    Returns:
        return value (bool) : True if successful, False otherwise.

    """

    # Sets the dictionary elements if dictionary is None.
    if database_params is None:
        database_params = {
            "csv_filename": "patient_metadata.csv",
            "database_filename": "us_patient_database.sqlite3",
            "data_directory": "data",
            "table_name": "tabular_data",
            "image_table_name": "images",
            "image_dir": "images",
            "overwrite": True,
            "hash_id": True,
        }

    # Establishes the database connection/creates the database
    connection = create_database_from_csv(database_params)

    # Creates a table for the images
    list_of_tables = [
        table[0]
        for table in connection.execute(
            "SELECT name FROM sqlite_master WHERE type='table';"
        )
    ]
    if (
        database_params["overwrite"]
        or database_params["image_table_name"] not in list_of_tables
    ):
        add_images_table_to_database(
            connection,
            image_table_name=database_params["image_table_name"],
            table_dict=table_dict,
        )

    # Adds the images to the database
    return_value = add_images_to_database(
        connection,
        database_params,
        table_dict,
    )

    return return_value


def main(config_file, log_level="WARNING"):
    """Main function for building the database.

    Args:
        config_file (str) : Path to the build database config file.
        log_level (str, optional) : Log level. Defaults to `WARNING`.

    Returns:
        Return value (bool) : True if successful, False otherwise.
    """

    # Sets up logger
    logger.remove()
    logger.add(sys.stderr, level=log_level)

    # Gets database parameters
    database_params = toml.load(config_file)["database_parameters"]
    table_dict = toml.load(config_file)["table_dict"]

    # Removes the log files (if they exists)
    for log_file in [
        database_params["log_failed_images"],
        database_params["log_not_linked"],
        database_params["log_duplicate_images"],
    ]:
        if os.path.exists(log_file):
            os.remove(log_file)

    # Builds the database
    return_value = build_database(database_params, table_dict=table_dict)

    return return_value


# Sets up the command line argument options
@click.command()
@click.option(
    "-c",
    "--config_file",
    default="/app/data/build_database.toml",
    help="Path to the toml file contain the database configuration.",
)
@click.option(
    "-l",
    "--log-level",
    default="WARNING",
    help="Log level.",
)
def click_main(
    config_file,
    log_level,
):
    """CLI wrapper for main function"""

    main(config_file, log_level=log_level)


if __name__ == "__main__":
    click_main()  # pylint: disable=no-value-for-parameter
