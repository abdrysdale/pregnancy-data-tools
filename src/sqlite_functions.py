""" A set of functions that interact with the sqlite3 database."""
#! /usr/bin/env python

# Python imports
import ast
from io import BytesIO
import sqlite3

# Module imports
import numpy as np
from loguru import logger


def connect_to_database(database="data/patient_data.sqlite3", timeout=30.0):
    """Connect to the SQLite database.

    Args:
        database (str, optional) : The path to the SQLite database.
        timeout (float, optional) : sqlite3 connect timeout. Defaults to 30.

    Returns:
        sqlite_connection (database connection) : SQLite database connection.
    """

    logger.debug(f"Trying to connect to {database}...\n")

    sqlite_connection = sqlite3.connect(database, timeout=timeout)
    cursor = sqlite_connection.cursor()
    version_query = "select sqlite_version()"
    cursor.execute(version_query)
    version_record = cursor.fetchall()
    logger.info(
        f"Connected to {database}!\n" f"SQLite Database Version is: {version_record}\n"
    )
    cursor.close()

    return sqlite_connection


def add_image_data_to_database(
    sqlite_connection,
    metadata_dict,
    image,
    table_name="images",
    headers=(
        "patient_id",
        "dateofexam",
        "gest_day",
        "image_number",
        "image_type",
        "image",
        "image_dims",
    ),
):
    """Adds image and extracted metadata to database

    Args:
        sqlite_connection (database connection) : SQLite database connection.
        metadata_dict (dict) : A dictionary containing the following:
            patient_id (int) : The patient id (foreign key).
            dateofexam (str) : The date of the examination.
            gest_day (int) : The number of gestation days.
            image_number (int) : The unique image number for a particular patient visit.
            image_type (int) : Either 0, 1 or 2 for right uterine,
                left uterine and umbilical respectively.
        image (ndarry) : A numpy array of the image file.
        table_name (str, optional) : The table name for the images.
            Defaults to 'images'.
        headers (list, optional) : A list of strings that are the table headers.
            Defaults to ("patient_id", "dateofexam", "gest_day",
            "image_number", "image_type", "image", "image_dims").

    Returns:
        return value (bool) : True if successful, False otherwise.
    """

    if not isinstance(headers, tuple):
        headers = tuple(headers)

    # Formats the image
    img_bytes = BytesIO()
    np.save(img_bytes, image, allow_pickle=True)

    # Converts the data entries into a single row entry.
    row_entry = [metadata_dict[h] for h in headers[:-2]]
    # Formats numpy arrays (if need be)
    for i, row in enumerate(row_entry):
        if isinstance(row, np.ndarray):
            row_bytes = BytesIO()
            np.save(row_bytes, row, allow_pickle=True)
            row_entry[i] = row_bytes.getvalue()
    row_entry.append(img_bytes.getvalue())
    row_entry.append(f"{image.shape}")

    # Insert row query
    insert_query = f"INSERT INTO {table_name}\n" f"{headers} VALUES ("
    insert_query += (len(row_entry) - 1) * "?, "
    insert_query += "?)"

    # Commits row to database
    cursor = sqlite_connection.cursor()
    cursor.execute(insert_query, row_entry)
    sqlite_connection.commit()
    cursor.close()

    return True


def get_from_db(connection, table, col_id, value):
    """Gets a value from a database

    For a given connection, returns an equivalent of the following:
        values = [v for v in col_id if v == value]

    If col_id and value are lists then will return the values where all constraints are
    met. That is, values where:
        values = col_id[0][:] == value[0] and ... and col_id[N][:] == value[N]
    where N is the length of the lists col_id and value.

    Args:
        sqlite_connection (database connection) : SQLite database connection.
        table (str) : Name of the table to be queried.
        col_id (str) : Column id to be queried.
            If `col_id` is a list, then expects `value` to be a list of the same length.
        value (str) : Value of query.
            If `value` is a list, then expects `col_id` to be a list of the same length.

    Returns:
        results (list) : A list of the matches.
    """

    # Formats value to a string (if needed)
    if not isinstance(value, str) and not isinstance(value, list):
        value = str(value)

    # Checks list formatting
    if isinstance(col_id, type(value)):
        if isinstance(value, list):
            if len(value) != len(col_id):
                logger.error(
                    f"Expected value {value} to be the same length as col_id {col_id}"
                )
                return -1
        else:
            col_id = [col_id]
            value = [value]
    else:
        logger.error(
            "Expected both col_id and value to be of the same type!\n"
            f"Instead col_id is {type(col_id)} and value is {type(value)}"
        )
        return -2

    query = f"Select * from {table} where {col_id[0]} = ?"
    for i in range(1, len(col_id)):
        query += f" AND {col_id[i]} = ?"

    params = tuple(value)
    cursor = connection.cursor()
    cursor.execute(query, params)
    results = cursor.fetchall()
    try:
        results = format_results(results)
    except TypeError as e:
        print(f"Error formatting results: {e}")
        logger.error(
            "Odd error: Expected both col_id and value to be of the same type!\n"
            f"Instead col_id is {type(col_id)} and value is {type(value)}"
        )
        return []  # Or some other appropriate default value

    return results


def format_results(results):
    """Convert binary entries to numpy arrays

    All other results are interpreted as literal_evals

    Args:
        results (list) : A list of lists containing each row returned from the query.

    Returns:
        formated_results (list) : Like results, but with binary elements converted to numpy
                arrays.
    """
    formated_results = []
    for row in results:
        formated_row = []
        for item in row:
            if isinstance(item, bytes):
                item_bytes = BytesIO(item)
                item_np = np.load(item_bytes, allow_pickle=True)
                if (
                    np.mean(
                        np.abs(item_np - (item_np.astype(np.uint8)).astype(np.float32))
                    )
                    < 1e-12
                ):
                    item_np = item_np.astype(np.uint8)
                formated_row.append(item_np)
            else:
                try:
                    formated_row.append(ast.literal_eval(item))
                except (SyntaxError, ValueError) as error:
                    logger.debug(error)
                    formated_row.append(item)

        formated_results.append(formated_row)
    return formated_results


if __name__ == "__main__":
    pass
