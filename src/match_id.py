"""Functions to match an ID to a database entry"""

#! /usr/bin/env python

# Module imports
from loguru import logger

# Local imports
from src import gen_hash
from src import sqlite_functions


def swap_chars(string, char, swap_char, pos=None):
    """Swaps a char in string for swap_char if at pos.

    Args:
        string (string) : String for character swapping.
        char (string) : Character in string to replace.
        swap_char (string) : Replacement character.
        pos (int, optional) : Position to peform swap.
            If None, will swap any character.
            Defaults to None.

    Return:
        swapped_string (string) : String with the swapped characters.
    """

    if isinstance(pos, int):
        pos = [pos]
    elif isinstance(pos, type(None)):
        pos = range(len(string))

    for i in pos:
        if string[i] == char:
            string = string[:i] + swap_char + string[i + 1 :]

    return string


def match_id(
    metadata,
    hash_key_dict,
    connection,
    database_params,
):
    """Tries to match the ID to one in the database

    Args:
        metadata (dict) : Metadata containing the following field:
            "patient_id" (str) : Patient ID obtained from image.
            Allow the fields in database_params["match_fields"].
        hash_key_dict (dict) : A dictionary of the hash keys.
        connection (sqlite3 connection) : Connection to the database.
        database_params (dict) : A dictionary containing the following fields:
           "hash_id" (bool) : Whether to hash the patient ID.
           "table_name" (str) : Name of the database table.
           "match_cols" (str, list) : The database columns to match.
           "match_fields" (str, list) : The metadata fields to match.

    Returns:
        patient_id (str) : The patient ID. If not matched to a database entry,
            will print a warning and write to a file.
    """

    temp_metadata = metadata.copy()

    def isin_db(sample_metadata):
        if database_params["hash_id"]:
            sample_metadata["patient_id"] = gen_hash.hash_data(
                sample_metadata["patient_id"],
                hash_key_dict,
            )

        if sqlite_functions.get_from_db(
            connection,
            database_params["table_name"],
            database_params["match_cols"],
            [sample_metadata[field] for field in database_params["match_fields"]],
        ):
            return True
        return False

    # Checks unmodified ID
    if isin_db(temp_metadata):
        return temp_metadata["patient_id"]

    # Checks swapping leading 0 with a D
    temp_metadata["patient_id"] = swap_chars(metadata["patient_id"], "0", "D", pos=0)
    if isin_db(temp_metadata):
        return temp_metadata["patient_id"]

    # Removes leading RW30 from ID
    temp_metadata["patient_id"] = metadata["patient_id"][4:]
    if isin_db(temp_metadata):
        return temp_metadata["patient_id"]

    # ID not matched, returns False and prints a warning
    logger.warning(f"ID: {metadata['patient_id']}, could not be matched.")
    return False
