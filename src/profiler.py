""" Profiles the build database scrip"""

#! /usr/bin/env python

# Python Imports
import cProfile
import pstats

# Local Imports
from src import build_database


def main():
    """Performs a profile of the build database code"""

    # Gets the test variables
    csv_filename = "test.csv"
    data_directory = "tests/test_data"
    database_filename = "test_db.sqlite3"
    image_dir = "tests/test_data/images/"
    image_table_name = "images"
    profile_file = "profile.prof"

    # Profiles the script
    profile = cProfile.Profile()
    profile.runcall(
        build_database.build_database,  # Function
        csv_filename=csv_filename,  # key word args (kwargs)
        data_directory=data_directory,
        database_filename=database_filename,
        image_dir=image_dir,
        image_table_name=image_table_name,
    )

    # Prints stats to stdout
    profile_stats = pstats.Stats(profile)
    profile_stats.sort_stats("cumtime")
    profile_stats.print_stats(0.1)

    # Writes stats to a profile file
    profile_stats.dump_stats(profile_file)


if __name__ == "__main__":
    main()
