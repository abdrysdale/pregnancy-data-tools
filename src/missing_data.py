"""Module for the functions to fill in missing data"""

#! /usr/bin/env python

# Python imports
import datetime

# Module imports
import pandas as pd
from loguru import logger

DUE_GEST_DAYS = 280
MAX_GEST_DAYS = 300
MIN_GEST_DAYS = 0


def check_gest(gest_days):
    """Checks if the gestation days falls within an acceptable value

    Args:
        gest_days (int) : Gestation days.

    Returns:
        return value (bool) : True if successful, else raises a ValueError.
    """
    if not MIN_GEST_DAYS < gest_days < MAX_GEST_DAYS:
        logger.critical(
            f"Gestation day {gest_days} doesn't fall within expected"
            f" range of {MIN_GEST_DAYS} < gest_days < {MAX_GEST_DAYS}"
        )
        raise ValueError("Gestation days does not fall within expected range")

    return True


def gest_from_date_and_due(date, due_date, check=True):
    """Calculate gestation days from a date and due date

    Args:
        date (datetime date object) : A date of particular scan.
        due_date (datetime date object) : Due date for the patient.
        check (bool) : Checks if the gestation days lies within an acceptable range.
            Defaults to True.
    Returns:
        gest_days (int) : Gestation days at time of exam.
    """

    gest_days = DUE_GEST_DAYS - (due_date - date).days
    if check:
        check_gest(gest_days)
    return int(gest_days)


def gest_from_date_and_prev_visit(date, prev_date, prev_gest, check=True):
    """Gets the gestation days from the date, previous date and previous due date.

    Args:
        date (datetime.date) : Date of exam.
        prev_date (datetime.date) : Previous date of exam.
        prev_gest (int) : Previous gestation days.
        check (bool) : Checks if the gestation days lies within an acceptable range.
            Defaults to True.

    Returns:
        gest_days (int) : Gestation days at time of exam.
    """

    gest_days = prev_gest + (date - prev_date).days
    if check:
        check_gest(gest_days)
    return int(gest_days)


def get_str2date_fn(date_str_format):
    """Gets a string to date format function

    Args:
        date_str_format (str) : The datetime string format.

    Returns:
        str2date_fn (function) : A function that converts strings to dates with the
            expected format.
    """

    def str2date_fn(date_str):
        """A string to date function

        Args:
            date_str (str) : A string to convert into a datetime object.

        Returns:
            date (datetime datetime object) : The string as a datetime object.
        """
        return datetime.datetime.strptime(date_str, date_str_format)

    return str2date_fn


def fill_missing_gest(data, db_params, prev_visit=False):
    """Fills in missing gestational day data

    Args:
        data (pandas dataframe) : A pandas dataframe.
        db_params (dict) : A dictionary of the following fields:
           gest_field (str) : Gestation day field name.
           date_field (str) : Date of exam field name.
           due_field (str) : Due date field name.
           id_field (str) : Patient ID field name.
        prev_visit (bool, optional) : Whether to calculate gestation days from previous
            patient visits.
            Defaults to False.

    Returns:
        data (pandas dataframe) : Dataframe with the missing gestation days filled in.
    """

    str2date = get_str2date_fn(db_params["date_format"])

    missing_data = data[pd.isna(data[db_params["gest_field"]])]
    try_due_and_date_method = (
        db_params["due_field"] in data.keys() and db_params["date_field"] in data.keys()
    )
    for index, row in missing_data.iterrows():
        if (
            try_due_and_date_method
            and not pd.isna(row[db_params["due_field"]])
            and not pd.isna(row[db_params["date_field"]])
        ):
            data[db_params["gest_field"]][index] = gest_from_date_and_due(
                str2date(row[db_params["date_field"]]),
                str2date(row[db_params["due_field"]]),
            )

    if prev_visit:
        missing_data = data[pd.isna(data[db_params["gest_field"]])]
        for index, row in missing_data.iterrows():
            if try_due_and_date_method:
                same_patient = data[
                    data[db_params["id_field"]] == row[db_params["id_field"]]
                ]
                for _, visit in same_patient.iterrows():
                    if not pd.isna(visit[db_params["gest_field"]]) and not pd.isna(
                        visit[db_params["date_field"]]
                    ):
                        data[db_params["gest_field"]][
                            index
                        ] = gest_from_date_and_prev_visit(
                            str2date(row[db_params["date_field"]]),
                            str2date(visit[db_params["date_field"]]),
                            visit[db_params["gest_field"]],
                        )
                        break

    return data
