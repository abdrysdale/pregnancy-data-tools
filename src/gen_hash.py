""" File for generating hashes """

# Python Imports
import os
import hashlib

# Module imports
import toml
import numpy as np
import pandas as pd
from loguru import logger


def hex2bytes(key_dict_hex):
    """Decodes the key dictionary from hexadecimal into bytes

    Args:
        key_dict_hex (dict) : key dictionary in hexadecimal format.

    Returns:
        key_dict_bytes (dict) : key dictionary in bytes format.
    """
    key_dict_bytes = {
        "digest_size": int(key_dict_hex["digest_size"]),
        "key": bytes.fromhex(key_dict_hex["key"]),
        "salt": bytes.fromhex(key_dict_hex["salt"]),
        "person": bytes.fromhex(key_dict_hex["person"]),
    }

    return key_dict_bytes


def bytes2hex(key_dict_bytes):
    """Encodes the key dictionary from bytes into hexadecimal

    Args:
        key_dict_bytes (dict) : key dictionary in bytes format.

    Returns:
        key_dict_hex (dict) : key dictionary in hexadecimal format.
    """
    key_dict_hex = {
        "digest_size": int(key_dict_bytes["digest_size"]),
        "key": key_dict_bytes["key"].hex(),
        "salt": key_dict_bytes["salt"].hex(),
        "person": key_dict_bytes["person"].hex(),
    }

    return key_dict_hex


def generate_keyfile(
    path,
    key=None,
    salt=None,
    person=None,
    digest_size=32,
):
    """Generates a key file for hashing.

    Args:
        path (str) : Path to store the key toml file.
        key (str) : Bytes-like object of the key.
            If None, then a key will be randomly generated.
            Defaults to None.
        salt (str) : Bytes-like object of the salt.
            If None, then a salt will be randomly generated.
            Defaults to None.
        person (str) : Bytes-like object of the person.
            If None, then a person will be randomly generated.
            Defaults to None.
        digest_size (int) : Digest size for hashing algorithm.
            Defaults to 32.

    Returns:
        key_dict (dict) : A dictionary containing the digest_size, key, salt and person.
    """

    if isinstance(key, type(None)):
        key = os.urandom(64)
    if isinstance(salt, type(None)):
        salt = os.urandom(16)
    if isinstance(person, type(None)):
        person = os.urandom(16)

    key_dict = {
        "digest_size": int(digest_size),
        "key": key,
        "salt": salt,
        "person": person,
    }

    with open(path, "w", encoding="utf-8") as file:
        toml.dump(bytes2hex(key_dict), file)

    return key_dict


def load_keyfile(path):
    """Loads key data from a toml file.

    Args:
        path (str) : Path to store the key toml file.

    Returns:
        key_dict (dict) : A dictionary containing the digest_size, key, salt and person.
    """
    key_dict = toml.load(path)
    return hex2bytes(key_dict)


def hash_data(data, key_dict, hash_csv=None):
    """Hashes data with the blake2b hash function according to the key_dict.

    Args:
        data (str) : Bytes-like string to be hashed.
            If data is not bytes-like then data will converted into the correct format.
        key_dict (dict) : A dictionary containing the 'digest_size', 'key', 'salt' and 'person'.
        hash_csv (str, optional) : Path to a csv file containing hash, data pairs.
            This is recommend if a look up table of hashes to original data is required.
            If None, the hash will not be written to any table.
            Naturally, the csv file needs to be kept secure.
            Defaults to None.

    Returns:
        hdata (str) : A hexadecimal representation of the hashed data.
    """

    if not isinstance(data, type(b"bytes")):
        data = bytes(str(data), "utf-8")

    hdata = hashlib.blake2b(
        data,
        digest_size=key_dict["digest_size"],
        key=key_dict["key"],
        salt=key_dict["salt"],
        person=key_dict["person"],
    )

    # Updates data to string format for saving to csv
    data = str(data)[2:-1]

    # Stores the hash to a file
    if not isinstance(hash_csv, type(None)):
        columns = ["Hash", "Data"]
        if not os.path.exists(hash_csv):
            hash_df = pd.DataFrame(data=None, index=None, columns=columns)
        else:
            hash_df = pd.read_csv(hash_csv)

        # Checks if data is already in the csv file.
        if not np.any(data == hash_df[columns[1]]):
            if not np.any(hdata.hexdigest() == hash_df[columns[0]]):
                hash_df.loc[-1] = [hdata.hexdigest(), data]
                hash_df.index = hash_df.index + 1
                hash_df = hash_df.sort_index()
                hash_df.to_csv(hash_csv, mode="w", encoding="utf-8", columns=columns)
            else:
                logger.critical(
                    "Hash found in table for different data!\n"
                    f"{data} has been hashed to {hdata.hexdigest()} "
                    "but hash has has already been used for:\n"
                    f"{hash_df[hash_df['Hash'] == hdata.hexdigest()]}\n"
                )
                raise ValueError("Hash is not unique")

    return hdata.hexdigest()


def get_keydict(path=None):
    """Gets the dictionary of key parameters.

    Args:
        path (str) : Path to the keyfile.
            If None, will default to ".key.toml".

    Returns:
        key_dict (dict) : A dictionary containing the 'digest_size', 'key', 'salt' and 'person'.
    """

    if isinstance(path, type(None)):
        path = ".key.toml"

    if not os.path.exists(path):
        key_dict = generate_keyfile(path)
    else:
        key_dict = load_keyfile(path)

    return key_dict
