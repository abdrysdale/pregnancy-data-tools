""" A set of image processing functions for preparing the ultrasound images """
#! /usr/bin/env python

# Python imports
import os
import multiprocessing as mp

# Module imports
import numpy as np
import PIL
from PIL import Image
from loguru import logger
from tqdm import tqdm

# Local imports
from src import cv_functions


def filename_increment(filename, previous_filenames):
    """Increments a filename if it already exists in previous_filenames

    Args:
        filename (str) : A filename of the format filename.ext.
        previous_filenames (str) : A list of previous filenames.

    Returns:
        new_filename (str) : If the filename is previous_filenames
            returns filename_i.txt, where i is the first number for which
            new_filename doesn't exist in the list.
            Else, returns filename.

    """

    # Sets up for while loop
    i = 1
    new_filename = filename

    # Checks if list is empty
    if len(previous_filenames) != 0:
        # While the new_filename is in the list append _i to filename and i++
        while new_filename in previous_filenames:
            new_filename = f"{filename.split('.')[0]}_{i}.{filename.split('.')[1]}"
            i += 1

    return new_filename


def get_mask(mask, image):
    """Gets an image mask from a mask dictionary

    Args:
        mask (dictionary) : A dictionary containing the mask "type" and "mask".
            The expected types are "array", "coords" or "templates".
            The expected mask for each type is, respectively:
                - A binary array of the same size of the image.
                - Coordinates of the format [y_start, y_end, x_start, x_end].
                - A path to image templates - see src/us_templates.toml for examples.
            For templates, the key_str and key_coords are used to identify the image
            template. The image_size is used to scale the image (if need be).
            The mask is applied to the namedob_coords coordinates.
            If None, the mask is set to
                { "type" : "templates", "mask" : "src/us_templates.toml"}
        image (ndarray) : An array of the image to be masked.

    Returns:
        img_mask (ndarry) : A boolean mask for the image.
    """

    # Assigns mask if mask is None
    if isinstance(mask, type(None)):
        mask = {"type": "templates", "mask": "src/us_templates.toml"}

    if mask["type"] == "templates":
        template_list = cv_functions.load_us_templates(
            mask["mask"],
            template_section="templates",
        )
        template = cv_functions.determine_image_template(image, template_list)
        mask = {
            "type": "coords",
            "mask": template["namedob_coords"],
        }

    if mask["type"] == "array":
        if mask["mask"].shape != image.shape:
            logger.critical(
                "Mask shape and image shape must be identical but got "
                f"{mask['mask'].shape} and {image.shape}, respectively."
            )
            raise ValueError("Incorrect mask size")
        img_mask = mask["mask"]

    elif mask["type"] == "coords":
        img_mask = np.ones(image.shape)
        img_mask[
            mask["mask"][0] : mask["mask"][1], mask["mask"][2] : mask["mask"][3]
        ] = 0

    else:
        logger.critical(f"Unknown mask type {mask['type']}")
        raise ValueError("Unknown mask type")

    return img_mask


def mask_patient_data(
    image,
    mask=None,
):
    """Masks the sensitive patient data

    Args:
        image (ndarray) : An ndarry of the patient ultrasound image.
        mask (dictionary, optional) : A dictionary containing the mask "type" and "mask".
            The expected types are "array", "coords" or "templates".
            The expected mask for each type is, respectively:
                - A binary array of the same size of the image.
                - Coordinates of the format [y_start, y_end, x_start, x_end].
                - A path to image templates - see src/us_templates.toml for examples.
            For templates, the key_str and key_coords are used to identify the image
            template. The image_size is used to scale the image (if need be).
            The mask is applied to the namedob_coords coordinates.
            If None, the mask is set to
                { "type" : "templates", "mask" : "src/us_templates.toml"}
            Defaults to None.

    Returns:
        masked_image (ndarry) : The masked image.

    """

    img_mask = get_mask(mask, image)

    return image * img_mask


def parallel_save_masked_image(args):
    """Parallel wrapper for save_masked_image"""
    print(args)
    return save_masked_image(*args)


def save_masked_image(
    image_src,
    image_dst,
    mask=None,
    **kwargs,
):
    """Loads in an image, performs the mask and saves the image.

    Args:
        image_src (str) : The path of the source image.
        image_dst (str) : The path for the masked image to be saved.
        mask (dictionary, optional) : A dictionary containing the mask "type" and "mask".
            The expected types are "array", "coords" or "templates".
            The expected mask for each type is, respectively:
                - A binary array of the same size of the image.
                - Coordinates of the format [y_start, y_end, x_start, x_end].
                - A path to image templates - see src/us_templates.toml for examples.
            For templates, the key_str and key_coords are used to identify the image
            template. The image_size is used to scale the image (if need be).
            The mask is applied to the namedob_coords coordinates.
            If None, the mask is set to
                { "type" : "templates", "mask" : "src/us_templates.toml"}
            Defaults to None.
        **kwargs log_file : The log file for failed images.

    Returns:
        return val (bool) : True if successful, False otherwise.
    """

    log_file = kwargs.get("log_file", "unknown_templates.log")

    # Loads the image
    try:
        image = np.array(Image.open(image_src))
    except PIL.UnidentifiedImageError:
        logger.warning(f"File {image_src} could not be masked as not an image.")
        return False

    # Performs the mask
    try:
        image = mask_patient_data(image, mask=mask)
    except ValueError as exception:
        if str(exception) == "Unknown Image Institute":
            logger.error(
                "Could not determine image template. "
                f"Image will not be saved to {image_dst}."
            )
            with open(log_file, "a", encoding="utf-8") as file:
                file.write(f"{image_src}\n")
            return False
        raise

    # Saves the result
    image = Image.fromarray(np.array(image, dtype=np.uint8))
    image.save(image_dst)

    return True


def get_file_arg_list(src_dir, dst_dir, mask, skip_df, preserve_structure):
    """Gets a list of file arguments for the batch anonymisation

    Args:
        src_dir (str) : The source directory containing the unmasked images.
        dst_dir (str) : The destination directory for the masked images.
        mask (dictionary, optional) : A dictionary containing the mask "type" and "mask".
            The expected types are "array", "coords" or "templates".
            The expected mask for each type is, respectively:
                - A binary array of the same size of the image.
                - Coordinates of the format [y_start, y_end, x_start, x_end].
                - A path to image templates - see src/us_templates.toml for examples.
            For templates, the key_str and key_coords are used to identify the image
            template. The image_size is used to scale the image (if need be).
            The mask is applied to the namedob_coords coordinates.
            If None, the mask is set to
                { "type" : "templates", "mask" : "src/us_templates.toml"}
            Defaults to None.
        skip_df (list, optional) : Directories/files to skip (include sub
            directories etc.) If None, won't skip any directories.
            Defaults to None.
        preserve_structure (bool) : If True, will preserve the directory
            structure, otherwise will flatten directory structure.
            Defaults to False.

    Returns:
        args (list) : A list of arguments for the save_mask_image function.

    """
    # Sets up the progress bar
    no_files = 0
    for _, dirs, files in os.walk(src_dir):
        dirs[:] = [d for d in dirs if d not in skip_df]
        files[:] = [file for file in files if file not in skip_df]
        no_files += len(files)
    pbar = tqdm(
        total=no_files,
        desc="Getting a list of files",
    )

    # For each file, performs a mask and saves to dst_dir
    previous_files = []
    args = []
    for root, dirs, files in os.walk(src_dir):
        dirs[:] = [d for d in dirs if d not in skip_df]
        files[:] = [file for file in files if file not in skip_df]
        for file in files:
            # Assumes all files are images
            if preserve_structure:
                image_dst = os.path.join(
                    dst_dir,
                    root[len(src_dir) :],
                    filename_increment(file, previous_files),
                )
                os.makedirs(
                    os.path.join(dst_dir, root[len(src_dir) :]),
                    exist_ok=True,
                )

            else:
                image_dst = os.path.join(
                    dst_dir,
                    filename_increment(file, previous_files),
                )

            args.append(
                (
                    os.path.join(root, file),
                    image_dst,
                    mask,
                )
            )
            previous_files.append(file)
            pbar.update(1)

    return args


def batch_mask_images(
    src_dir,
    dst_dir,
    mask=None,
    **kwargs,
):
    """Masks and saves all images in a directory and saves in destination dir

    Args:
        src_dir (str) : The source directory containing the unmasked images.
        dst_dir (str) : The destination directory for the masked images.
        mask (dictionary, optional) : A dictionary containing the mask "type" and "mask".
            The expected types are "array", "coords" or "templates".
            The expected mask for each type is, respectively:
                - A binary array of the same size of the image.
                - Coordinates of the format [y_start, y_end, x_start, x_end].
                - A path to image templates - see src/us_templates.toml for examples.
            For templates, the key_str and key_coords are used to identify the image
            template. The image_size is used to scale the image (if need be).
            The mask is applied to the namedob_coords coordinates.
            If None, the mask is set to
                { "type" : "templates", "mask" : "src/us_templates.toml"}
            Defaults to None.
        **kwargs: skip_df (list, optional) : Directories/files to skip (include sub
            directories etc.) If None, won't skip any directories.
            Defaults to None.
        **kwargs: preserve_structure (bool) : If True, will preserve the directory
            structure, otherwise will flatten directory structure.
            Defaults to False.
        ***kwargs: parallel (bool) : If True, performs in parallel.
            Defaults to True.

    Returns:
        return value (bool) : True if successful, False otherwise.
    """

    # Set keyword arguments
    skip_df = kwargs.get("skip_df", None)
    preserve_structure = kwargs.get("preserve_structure", False)
    parallel = kwargs.get("parallel", True)

    # Formats skip_dirs to a list (if need be)
    # Assumes single entry if not a list
    if not isinstance(skip_df, list):
        skip_df = [skip_df]

    # Gets a list of files
    arg_list = get_file_arg_list(src_dir, dst_dir, mask, skip_df, preserve_structure)

    # Anonymises the images
    if parallel:
        no_usable_processes = mp.cpu_count() - 1
        logger.info(f"Using {no_usable_processes} processes.")
        with mp.Pool(no_usable_processes) as pool:
            outputs = list(
                tqdm(
                    pool.imap(
                        parallel_save_masked_image,
                        arg_list,
                    ),
                    total=len(arg_list),
                    desc="Anonymising images",
                )
            )
    else:
        pbar = tqdm(
            total=len(arg_list),
            desc="Anonymising images",
        )
        outputs = []
        for args in arg_list:
            outputs.append(save_masked_image(*args))
            pbar.update(1)

        pbar.n = pbar.total
        pbar.refresh()

    logger.info(f"Success rate: {np.sum(outputs)/len(arg_list) * 100}%")

    return True
