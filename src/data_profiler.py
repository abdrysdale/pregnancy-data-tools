"""A simple script that runs a pandas profiler tool

The tool can be found at: https://github.com/pandas-profiling/pandas-profiling
"""
#! /usr/bin/env python3

# Module Imports
import pandas as pd
from pandas_profiling import ProfileReport


def profile_data(filename="data/CONFIDENTIAL/patient_data.xls"):
    """Profiles filename using a pandas profiling

    Args:
        filename (str, optional) : The excel file to profile.
            Defaults to "data/CONFIDENTIAL/patient_data.xls".

    Returns:
        return value (bool) : True if successful, False otherwise.
    """

    data = pd.read_excel(filename)
    profile = ProfileReport(data, title="Pregnancy Data Profile Report")
    profile.to_file("data/CONFIDENTIAL/pregnancy_data_profile_report.html")

    return True


if __name__ == "__main__":
    profile_data()
