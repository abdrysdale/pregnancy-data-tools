""" Tests for the sqlite_functions.py file."""

# /usr/bin/env python

# Python imports
import sys
import os
import ast

# Module imports
import numpy as np
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

from src import sqlite_functions
from src import build_database
from src import gen_hash


def test_connect_to_database():
    """Tests the connect_to_database function"""

    database_path = "tests/test_data/test_db.sqlite3"
    sqlite_connection = sqlite_functions.connect_to_database(database=database_path)

    assert True


def test_add_image_data_to_database():
    """Tests adding image metadata to database."""

    # Gets the database connection
    database_path = "tests/test_data/test_db.sqlite3"
    sqlite_connection = sqlite_functions.connect_to_database(database=database_path)
    table_name = "images"

    # Creates test data
    data_dict = {
        "patient_id": 1,
        "dateofexam": "01/01/1970",
        "gest_day": 100,
        "vessel_type": 0,
        "image_number": 1,
    }
    headers = list(data_dict.keys())
    headers.append("image")
    headers.append("image_dims")

    image = np.random.rand(1, 10, 10)

    rtn_val = sqlite_functions.add_image_data_to_database(
        sqlite_connection,
        data_dict,
        image,
        table_name=table_name,
        headers=headers,
    )
    assert rtn_val

    # Checks the stored result
    row = sqlite_functions.get_from_db(
        sqlite_connection,
        table_name,
        col_id="patient_id",
        value=1,
    )
    row = row[-1]
    image_dims = row[-1]
    return_image = np.frombuffer(row[-2]).reshape(image_dims)
    assert np.all(return_image == image)

    for key in data_dict.keys():
        idx = np.where([h == key for h in headers])[0][0]
        assert data_dict[key] == row[idx]


def test_get_from_db():
    # Gets the database connection
    database_path = "tests/test_data/test_db.sqlite3"
    con = sqlite_functions.connect_to_database(database=database_path)
    table = "table_data"
    col_id = "patientid"
    value = "67508e97e53a35984d05439fa8976508278800845ee51292f49829e606c3ecbb"

    results = sqlite_functions.get_from_db(con, table, col_id, value)
    assert results

    col_ids = ["patientid", "usgestdays", "date", "clinic"]
    values = [
        "015916e41e51dec221d47a13bbc514e28a41a9ae3a070ce2e6ce66082cf89795",
        173,
        "28/03/2012",
        "MAViS",
    ]
    results = sqlite_functions.get_from_db(con, table, col_ids, values)
    assert results


if __name__ == "__main__":
    logger.debug("Running tests on sqlite_functions.py ...\n")
    test_connect_to_database()
    test_add_image_data_to_database()
    test_get_from_db()
    logger.info("sqlite_functions passed!\n")
