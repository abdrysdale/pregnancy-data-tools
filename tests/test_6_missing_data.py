"""Tests for missing_data module"""

# /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import pandas as pd
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

from src import missing_data


def test_fill_missing_gest():
    fields = {
        "gest_field": "usgestdays",
        "date_field": "date",
        "due_field": "duedate",
        "id_field": "patientid",
        "date_format": "%Y/%m/%d",
    }

    data = pd.read_csv("tests/test_data/missing_data.csv")

    filled_data = missing_data.fill_missing_gest(
        data,
        fields,
        prev_visit=True,
    )

    correct_gest_days = [
        173.0,
        185.0,
        199.0,
        161.0,
        211.0,
        184.0,
        169.0,
        175.0,
        255.0,
        286.0,
        235.0,
        206.0,
    ]

    for idx, row in filled_data.iterrows():
        assert row[fields["gest_field"]] == correct_gest_days[idx]


if __name__ == "__main__":
    logger.debug("Testing missing_data module")
    test_fill_missing_gest()
    logger.info("missing_data module passed!")
