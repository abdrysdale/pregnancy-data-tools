"""Tests for match_id module"""

#! /usr/bin/env python

# Module imports
from loguru import logger

# Local imports
from src import match_id


def test_swap_chars():
    """Tests swap chars function"""

    string = "string"
    char = "i"
    swap_char = "o"

    assert match_id.swap_chars(string, char, swap_char, pos=None) == "strong"
    assert match_id.swap_chars(string, char, swap_char, pos=0) == "string"
    assert match_id.swap_chars(string, char, swap_char, pos=3) == "strong"

    string = "foo"
    char = "o"
    swap_char = "a"
    assert match_id.swap_chars(string, char, swap_char, pos=None) == "faa"


if __name__ == "__main__":
    logger.debug("Testing match_id module...")
    test_swap_chars()
    logger.info("match_id passed!")
