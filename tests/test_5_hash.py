""" Tests for the gen_hash.py file."""

# /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pandas as pd
import pytest
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

from src import gen_hash


def test_get_keydict():
    path = "tests/.key.toml"
    key = b"key"
    salt = b"salt"
    person = b"person"
    digest_size = 32

    key_dict = gen_hash.generate_keyfile(
        path, key=key, salt=salt, person=person, digest_size=digest_size
    )

    assert key_dict["key"] == key
    assert key_dict["salt"] == salt
    assert key_dict["person"] == person

    key_dict = gen_hash.get_keydict(path)

    assert key_dict["key"] == key
    assert key_dict["salt"] == salt
    assert key_dict["person"] == person


def test_hash_data():
    path = "tests/.key.toml"
    key_dict = gen_hash.get_keydict(path)
    data = "test_id"

    # Makes sure there is a new csv file for each run of the test
    csv_path = "tests/.key.csv"
    if os.path.exists(csv_path):
        os.remove(csv_path)

    # Test hash_data function runs
    assert gen_hash.hash_data(data, key_dict)

    # Test saving to csv file
    hashed_data = gen_hash.hash_data(data, key_dict, hash_csv=csv_path)

    # Tests catching non-unique hashes
    hashed_df = pd.read_csv(csv_path)
    hashed_df["Data"] = "not_test_id"
    hashed_df.to_csv(csv_path, mode="w", encoding="utf-8", columns=["Hash", "Data"])
    with pytest.raises(ValueError) as except_info:
        gen_hash.hash_data(data, key_dict, hash_csv=csv_path)
    assert str(except_info.value) == "Hash is not unique"


if __name__ == "__main__":
    logger.debug("Testing gen_hash.py ...")
    test_get_keydict()
    test_hash_data()
    logger.info("Great news, gen_hash.py passed all tests!")
