""" Tests for the image_processing.py file."""
# /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
import pytest
from PIL import Image
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from src import image_processing


def test_filename_increment():
    # Name not in names
    names = ["file.txt", "image.png", "database.db"]
    name = "test.csv"
    new_name = image_processing.filename_increment(name, names)
    assert name == new_name

    # Name in names
    names = ["file.txt", "file_1.txt"]
    name = "file.txt"
    new_name = image_processing.filename_increment(name, names)
    assert new_name == "file_2.txt"

    # Empty list
    names = []
    name = "test.csv"
    new_name = image_processing.filename_increment(name, names)
    assert name == new_name


def test_mask_patient_data():
    # Tests with a custom mask
    image_size = np.array([100, 100])
    unmasked_image = np.ones(image_size)
    mask = np.random.randint(0, high=2, size=image_size)
    mask_dict = {"type": "array", "mask": mask}
    masked_image = image_processing.mask_patient_data(unmasked_image, mask=mask_dict)
    assert np.all(unmasked_image * mask == masked_image)

    # Tests with a too large shaped custom mask
    mask = np.random.randint(0, high=1, size=image_size + 1)
    mask_dict = {"type": "array", "mask": mask}
    with pytest.raises(Exception) as e:
        masked_image = image_processing.mask_patient_data(
            unmasked_image, mask=mask_dict
        )
    assert str(e.value) == "Incorrect mask size"

    # Tests with mask coordinates
    mask_coords = [0, unmasked_image.shape[0], 0, unmasked_image.shape[1]]
    mask_dict = {"type": "coords", "mask": mask_coords}
    masked_image = image_processing.mask_patient_data(unmasked_image, mask=mask_dict)
    assert np.all(masked_image == np.zeros(masked_image.shape))


def test_save_masked_image():
    # Image path
    image_path = "tests/test_data/images/left_ut_image.png"
    image_dest = image_path

    assert image_processing.save_masked_image(image_path, image_dest) == True


def test_batch_mask_images():
    src_dir = "tests/test_data/images/"
    dst_dir = "tests/test_data/more_images/"
    assert image_processing.batch_mask_images(src_dir, dst_dir) == True


if __name__ == "__main__":
    logger.debug("Testing image_processing.py ...")
    test_filename_increment()
    test_mask_patient_data()
    test_save_masked_image()
    test_batch_mask_images()
    logger.info("image_processing.py passed!")
