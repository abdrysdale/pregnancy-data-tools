#!/usr/bin/env python
""" Tests for the build_database.py file."""

# Python imports
import sys
import os
from PIL import Image
# TESSDATA_PREFIX is always found to be wrong for me, so force it here:
os.environ['TESSDATA_PREFIX'] = r'C:\Program Files\Tesseract-OCR\tessdata'

# Module imports
import numpy as np
import toml
from loguru import logger

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

from src import build_database
from src import sqlite_functions


def test_create_database_from_csv():
    # Sets up test database
    toml_file = "tests/test_data/build_database.toml"
    database_params = toml.load(toml_file)["database_parameters"]

    connection = build_database.create_database_from_csv(database_params)

    assert connection != None

    database_params["overwrite"] = False
    connection = build_database.create_database_from_csv(database_params)


def test_add_images_table_to_database():
    # Sets up test database
    toml_file = "tests/test_data/build_database.toml"
    database_params = toml.load(toml_file)["database_parameters"]

    connection = build_database.create_database_from_csv(database_params)

    # Adds the image table
    image_table_name = "images"
    return_val = build_database.add_images_table_to_database(
        connection,
        image_table_name=image_table_name,
    )

    assert return_val is True


def test_main():
    """Tests the main function"""

    toml_file = "tests/test_data/build_database.toml"
    not_linked_file = toml.load(toml_file)["database_parameters"]["log_not_linked"]
    if os.path.exists(not_linked_file):
        os.remove(not_linked_file)

    assert build_database.main(
        toml_file,
        log_level="DEBUG",
    )

    assert not os.path.exists(not_linked_file)

    # Tests loading of images
    database_params = toml.load(toml_file)["database_parameters"]
    connection = sqlite_functions.connect_to_database(database_params["database_path"])
    row = sqlite_functions.get_from_db(
        connection,
        database_params["image_table_name"],
        "gest_day",
        201,
    )[0]
    img_db = row[-2]

    img = np.array(Image.open("/app/tests/test_data/images/st_marys/st_marys.jpg"))

    img = img[100:, :, :]  # Removes patient id as unmasked in orginal image
    img_db = img_db[100:, :, :]

    assert np.mean(np.abs(img - img_db)) < 1e-9


if __name__ == "__main__":
    logger.debug("Testing build_database.py ...")
    test_create_database_from_csv()
    test_add_images_table_to_database()
    test_main()
    logger.info("Great news, build_database.py passed all tests!")
