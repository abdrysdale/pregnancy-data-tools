""" Tests for the cv_functions.py file."""
# /usr/bin/env python

# Python imports
import sys
import os

# Module imports
import numpy as np
from PIL import Image
from loguru import logger
import toml

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from src import cv_functions


# Tests the loading of the TOML file
def test_load_us_templates():
    toml_file = "src/us_templates.toml"
    template_list = cv_functions.load_us_templates(toml_file)

    logger.debug(f"Loaded the following templates:{template_list}\n")

    assert template_list[0]["name"] == "MAViS"
    assert template_list[0]["patient_id_coords"] == [28, 47, 45, 88]
    assert template_list[0]["gest_day_coords"] == [28, 47, 181, 210]
    assert template_list[0]["dateofexam_coords"] == [28, 47, 493, 553]
    assert template_list[0]["vessel_type_coords"] == [49, 60, 512, 520]


def test_scale_image_template():
    # Sets up image and template for scaling
    template = {
        "key_coords": [10, 10, 20, 20],
        "patient_id_coords": [10, 10, 20, 20],
        "gest_day_coords": [10, 10, 20, 20],
        "dateofexam_coords": [10, 10, 20, 20],
        "vessel_type_coords": [10, 10, 20, 20],
        "image_size": [50, 100],
    }
    image = np.ones((100, 200))

    new_template = cv_functions.scale_image_template(image, template)

    assert np.all(new_template["key_coords"] == [20, 20, 40, 40])
    assert np.all(new_template["patient_id_coords"] == [20, 20, 40, 40])
    assert np.all(new_template["gest_day_coords"] == [20, 20, 40, 40])
    assert np.all(new_template["dateofexam_coords"] == [20, 20, 40, 40])
    assert np.all(new_template["vessel_type_coords"] == [20, 20, 40, 40])

    # Sets up image and template for no scaling
    template = {
        "key_coords": [10, 10, 20, 20],
        "patient_id_coords": [10, 10, 20, 20],
        "gest_day_coords": [10, 10, 20, 20],
        "dateofexam_coords": [10, 10, 20, 20],
        "vessel_type_coords": [10, 10, 20, 20],
        "image_size": [50, 100],
    }
    image = np.ones((50, 100))

    new_template = cv_functions.scale_image_template(image, template)

    assert np.all(new_template["key_coords"] == [10, 10, 20, 20])
    assert np.all(new_template["patient_id_coords"] == [10, 10, 20, 20])
    assert np.all(new_template["gest_day_coords"] == [10, 10, 20, 20])
    assert np.all(new_template["dateofexam_coords"] == [10, 10, 20, 20])
    assert np.all(new_template["vessel_type_coords"] == [10, 10, 20, 20])


# Determines the image institute
def test_determine_image_template():
    # Loads the templates
    toml_file = "src/us_templates.toml"
    template_list = cv_functions.load_us_templates(toml_file)

    # Loads the image
    image_path = "tests/test_data/images/IMG_20130523_104_13.jpg"
    image = np.array(Image.open(image_path))

    # Gets the institute template
    institute_template = cv_functions.determine_image_template(image, template_list)
    assert institute_template["name"] == "MAViS"

    # Loads the image
    image_path = "tests/test_data/images/st_marys/st_marys.jpg"
    image = np.array(Image.open(image_path))

    # Gets the institute template
    institute_template = cv_functions.determine_image_template(image, template_list)
    assert institute_template["name"] == "Manchester Placenta Clinic"

    logger.info("Image institute successfully determined.")


# Tests the conversion from gestation days string to an int
def test_gest_str_to_days():
    gest_day_str = "19w6"
    gest_day_int = cv_functions.gest_str_to_days(gest_day_str)
    assert gest_day_int == 139

    gest_day_str = "4w0"
    gest_day_int = cv_functions.gest_str_to_days(gest_day_str)
    assert gest_day_int == 28


def test_get_text_from_us():
    image_path = "tests/test_data/images/IMG_20130523_104_13.jpg"
    image = np.array(Image.open(image_path))

    # Gets the config files
    patient_id_config = "src/tesseract_config/patient_id.txt"
    gest_day_config = "src/tesseract_config/gest_day.txt"
    dateofexam_config = "src/tesseract_config/dateofexam.txt"
    vessel_type_config = "src/tesseract_config/vessel_type.txt"

    # Gets the coordinates [y_start, y_fin, x_start, x_fin]
    patient_id_coords = [28, 47, 45, 88]
    gest_day_coords = [28, 47, 181, 210]
    dateofexam_coords = [28, 47, 493, 553]
    vessel_type_coords = [49, 60, 512, 520]

    # Gets the cropped images
    patient_id_img = image[
        patient_id_coords[0] : patient_id_coords[1],
        patient_id_coords[2] : patient_id_coords[3],
    ]
    gest_day_img = image[
        gest_day_coords[0] : gest_day_coords[1],
        gest_day_coords[2] : gest_day_coords[3],
    ]
    dateofexam_img = image[
        dateofexam_coords[0] : dateofexam_coords[1],
        dateofexam_coords[2] : dateofexam_coords[3],
    ]
    vessel_type_img = image[
        vessel_type_coords[0] : vessel_type_coords[1],
        vessel_type_coords[2] : vessel_type_coords[3],
    ]

    # Gets the text from the images
    patient_id_text = cv_functions.get_text_from_us(
        patient_id_img,
        config_file=patient_id_config,
    )
    gest_day_text = cv_functions.get_text_from_us(
        gest_day_img,
        config_file=gest_day_config,
    )
    dateofexam_text = cv_functions.get_text_from_us(
        dateofexam_img,
        config_file=dateofexam_config,
    )
    vessel_type_text = cv_functions.get_text_from_us(
        vessel_type_img,
        config_file=vessel_type_config,
        scale_factor=4,
    )

    # Informs of output
    logger.info(
        f"Extracted the following information from {image_path}\n"
        f"patient_id:\t{patient_id_text}\n"
        f"gest_day:\t{gest_day_text}\n"
        f"dateofexam:\t{dateofexam_text}\n"
        f"vessel_type:\t{vessel_type_text}\n"
    )

    # Checks results
    # assert patient_id_text == "D55364"
    assert gest_day_text == "24w5"
    assert dateofexam_text == "28.03.2012"
    assert vessel_type_text == "L"


# Test the extraction of metadata from ultrasound
def test_get_us_metadata():
    # Loads the image
    image_path = "tests/test_data/images/IMG_20130523_104_13.jpg"
    image = np.array(Image.open(image_path))

    metadata = cv_functions.get_us_metadata(image)

    logger.debug(f"Extracted the following metadata:\n{metadata}")

    # Checks results
    # assert metadata["patient_id"] == "D55364"
    assert metadata["gest_day"] == 173
    assert metadata["dateofexam"] == "28/03/2012"
    assert metadata["vessel_type"] == 1

    # Loads the image
    image_path = "tests/test_data/images/st_marys/st_marys.jpg"
    image = np.array(Image.open(image_path))

    metadata = cv_functions.get_us_metadata(image)

    logger.debug(f"Extracted the following metadata:\n{metadata}")

    # Checks results
    # assert metadata["patient_id"] == "RW301727565"
    assert metadata["gest_day"] == 201
    assert metadata["dateofexam"] == "28/04/2021"
    assert metadata["vessel_type"] == 0


def test_get_labelled_vessel_type():
    """Tests the converting labelled vessel type function"""

    # Defines the vessel type
    umb = 0
    left_ut = 1
    right_ut = 2
    no_text_extracted = -1
    unknown_error = -2

    assert cv_functions.get_labelled_vessel_type("U") == umb
    assert cv_functions.get_labelled_vessel_type("Um") == umb
    assert cv_functions.get_labelled_vessel_type("umbilical") == umb

    assert cv_functions.get_labelled_vessel_type("L") == left_ut
    assert cv_functions.get_labelled_vessel_type("Le") == left_ut
    assert cv_functions.get_labelled_vessel_type("left uterine") == left_ut

    assert cv_functions.get_labelled_vessel_type("R") == right_ut
    assert cv_functions.get_labelled_vessel_type("Rt") == right_ut
    assert cv_functions.get_labelled_vessel_type("right uterine") == right_ut

    assert cv_functions.get_labelled_vessel_type("N/A") == no_text_extracted
    assert cv_functions.get_labelled_vessel_type("some unknown error") == unknown_error


def test_segment_doppler():
    # Images that comes with the usseg library
    image_path = "tests/test_data/lt_openaccess.png"
    image = np.array(Image.open(image_path))

    # Currently the textual extraction fails on this image but text extraction
    # from usseg is not being used - just the signal extraction
    textdf, (t_axis, signal) = cv_functions.segment_doppler(image)
    logger.info(f"Extracted the following from {image_path}:\n{textdf}")

    assert t_axis
    assert signal

    # Anonymised image from the database
    image_path = "tests/test_data/images/left_ut_image.png"

    textdf, (t_axis, signal) = cv_functions.segment_doppler(image)
    logger.info(f"Extracted the following from {image_path}:\n{textdf}")

    assert t_axis
    assert signal


if __name__ == "__main__":
    logger.debug("Testing CV functions...")
    test_get_labelled_vessel_type()
    test_load_us_templates()
    test_scale_image_template()
    test_determine_image_template()
    test_gest_str_to_days()
    test_get_text_from_us()
    test_get_us_metadata()
    test_segment_doppler()
    logger.debug("CV functions passed!")
